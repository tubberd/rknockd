#![feature(await_macro, async_await, arbitrary_self_types)]

/// Contains the CLI.
pub mod cli;

/// Contains the listener module.
pub mod listener;

/// Module containing config
/// logic
pub mod config;

use std::fs::*;
use std::io::Read;
use std::collections::HashSet;
use std::sync::Arc;

use config::*;
use listener::*;

use toml::from_slice;
use log::*;
use simplelog::{
    SimpleLogger,
    LevelFilter,
    Config as LogConfig
};
use futures::executor::{
    self,
    ThreadPool
};

/// Reads the config file from a given path
/// # Params
/// * `path`    Path to the config file
pub fn read_config(path: String) -> Config {
    let mut file = File::open(path).expect("ERROR! Could not open config file.");
    let mut bytes: Vec<u8> = Vec::new();
    let _ = file.read_to_end(&mut bytes).expect("ERROR! Could not read config file.");
    from_slice(&bytes).expect("ERROR! Could not parse config file.")
}

/// Sets up the logging facilities
pub fn set_up_logging() {
    SimpleLogger::init(LevelFilter::Info, LogConfig::default()).unwrap();
}

fn main() {
    // Build and load CLI
    let app = cli::build_cli();
    let app_matches = app.get_matches();
    // Load config file
    let config_path = String::from(app_matches.value_of("config").unwrap());
    let config = read_config(config_path);
    // Set up logging
    set_up_logging();
    // Check flags
    let udp_or_tcp = app_matches.is_present("udp") || app_matches.is_present("tcp");
    let udp_and_tcp = app_matches.is_present("udp") && app_matches.is_present("tcp");
    if !udp_or_tcp {
        error!("You need to specify the protocol the daemon listens for!");
        std::process::exit(-1);
    }
    if udp_and_tcp {
        error!("You need to specify either one of the protocols!");
        std::process::exit(-2);
    }
    // Determine protocol/mode
    let mode;
    if app_matches.is_present("udp") {
        info!("Starting daemon in UDP mode...");
        mode = ListenerMode::UDP;
    } else if app_matches.is_present("tcp") {
        error!("TCP support is not implemented yet!");
        mode = ListenerMode::TCP;
        std::process::exit(-3);
    } else {
        info!("Starting daemon in UDP mode...");
        mode = ListenerMode::UDP;
    }

    // Get set of ports to listen on
    let mut ports: HashSet<u16> = HashSet::new();
    for (_, trigger) in &config.triggers {
        for port in trigger.sequence.iter() {
            ports.insert(*port);
        }
    }

    info!("Starting package sniffer on interface {}...", config.interface);
    let listener_raw = Listener::new(config.interface.clone(), ports, mode);
    let listener = listener_raw.start();
    info!("Stopping package sniffer...");

    executor::block_on(async {
        main_loop(listener).await;
    });
}

pub async fn main_loop(listener: Arc<Listener>) {
    loop {
        info!("Main loop iteration!");
        let _ = listener.get_package().await;
    }
}