use std::collections::{
    VecDeque,
    HashSet
};
use std::thread::{
    spawn,
    JoinHandle
};
use std::ops::Deref;
use std::sync::{
    Arc,
    RwLock
};
use std::pin::Pin;
use std::task::Context;

use crate::config::*;

use log::*;
use pnet::datalink::{self, NetworkInterface};

use pnet::packet::arp::ArpPacket;
use pnet::packet::ethernet::{EtherTypes, EthernetPacket, MutableEthernetPacket};
use pnet::packet::icmp::{echo_reply, echo_request, IcmpPacket, IcmpTypes};
use pnet::packet::icmpv6::Icmpv6Packet;
use pnet::packet::ip::{IpNextHeaderProtocol, IpNextHeaderProtocols};
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::ipv6::Ipv6Packet;
use pnet::packet::tcp::TcpPacket;
use pnet::packet::udp::UdpPacket;
use pnet::packet::Packet;
use pnet::util::MacAddr;
use futures::prelude::*;
use futures::task::{
    Poll
};

/// Listener that returns when packets of certain ports have been hit
pub struct Listener {
    interface: String,
    ports: HashSet<u16>,
    worker: RwLock<Option<JoinHandle<()>>>,
    running: RwLock<bool>,
    pub incoming_packets: RwLock<VecDeque<IncomingPacket>>,
    mode: ListenerMode,
}

pub enum ListenerMode {
    UDP,
    TCP
}

pub struct IncomingPacket {
    pub ip: String,
    pub port: u16,
}

impl IncomingPacket {
    /// Creates a new incoming packet future from a listener
    pub fn new(ip: String, port: u16) -> Self {
        Self {
            ip: ip,
            port: port
        }
    }
}

pub struct PacketListener {
    listener: Arc<Listener>
}

impl PacketListener {
    /// Creates a new incoming packet listener
    pub fn new(listener: Arc<Listener>) -> Self {
        Self {
            listener: listener
        }
    }
}

impl Future for PacketListener {
    type Output = IncomingPacket;

    fn poll(self: Pin<&mut Self>, _: &mut Context) -> Poll<Self::Output> {
        let plisten = self.deref();
        let listener = &plisten.listener;
        let mut incoming_packets = listener.incoming_packets.write().unwrap();
        if incoming_packets.len() > 0 {
            let packet = incoming_packets.pop_front().unwrap();
            return Poll::Ready(packet);
        } else {
            return Poll::Pending;
        }
    }
}

impl Listener {
    /// Creates a new listener for a set of ports and protocol
    /// # Params
    /// * `ports`   A set of ports to listen on
    /// * `mode`    The protocol to use
    pub fn new(interface: String, ports: HashSet<u16>, mode: ListenerMode) -> Self {
        Self {
            interface: interface,
            ports: ports,
            incoming_packets: RwLock::new(VecDeque::new()),
            worker: RwLock::new(None),
            running: RwLock::new(false),
            mode: mode
        }
    }

    /// Starts the background packet sniffer
    pub fn start(self) -> Arc<Self> {
        {
            let mut running = self.running.write().unwrap();
            *running = true;
        }
        let arc_self = Arc::new(self);
        let arc_cpy = arc_self.clone();
        let handle = spawn(move || {
            arc_cpy.work();
        });
        *arc_self.worker.write().unwrap() = Some(handle);
        arc_self
    }

    /// Stops the background packet sniffer and joins the worker thread.
    pub fn stop(&self) {
        {
            let mut running = self.running.write().unwrap();
            *running = false;
        }
        if self.worker.read().unwrap().is_some() {
            let mut handle_guard = self.worker.write().unwrap();
            let handle = handle_guard.take().unwrap();
            let _ = handle.join();
        }
    }

    /// Returns a Future containing a new incoming package
    pub fn get_package(self: &Arc<Self>) -> impl Future {
        PacketListener::new(self.clone())
    }

    /// Performs background work.
    fn work(&self) {
        use pnet::{
            *,
            packet::ethernet::EthernetPacket,
            packet::ip::*,
            packet::ipv4::*,
            packet::ipv6::*,
            packet::udp,
            datalink::*,
            datalink::Channel::Ethernet
        };

        let mut running = {
            *self.running.read().unwrap()
        };

        let interface_names_match = | iface: &NetworkInterface | iface.name == self.interface;
        let interfaces = datalink::interfaces();
        let interface_opt = interfaces.into_iter()
            .filter(interface_names_match)
            .next();

        if interface_opt.is_none() {
            error!("Invalid network interface!");
            std::process::exit(-4);
        }

        let interface = interface_opt.unwrap();
        let channel_res = datalink::channel(&interface, Default::default());
        
        let mut receiver = match channel_res {
            Ok(Ethernet(tx, rx)) => { rx },
            _ => {
                error!("Error creating datachannel!");
                std::process::exit(-5);
            }
        };

        while running {
            running = {
                *self.running.read().unwrap()
            };
            match receiver.next() {
                Ok(packet) => {
                    let ether_packet_opt = EthernetPacket::new(packet);
                    if ether_packet_opt.is_none() {
                        error!("Error parsing datalink package!");
                    }
                    let ether_packet = ether_packet_opt.unwrap();
                    self.handle_packet(ether_packet);
                },
                _ => {
                    error!("Unknown datalink package!");
                }
            }
        }
    }

    fn handle_packet(&self, packet: EthernetPacket) {
        info!("Handling package!");
        match packet.get_ethertype() {
            EtherTypes::Ipv4 => {
                let packet_opt = Ipv4Packet::new(packet.payload());
                if packet_opt.is_none() {
                    error!("Error parsing IPv4 packet!");
                    return;
                }
                let packet = packet_opt.unwrap();
                self.handle_ipv4_packet(packet);
            },
            EtherTypes::Ipv6 => {
                let packet_opt = Ipv6Packet::new(packet.payload());
                if packet_opt.is_none() {
                    error!("Error parsing IPv6 packet!");
                    return;
                }
                let packet = packet_opt.unwrap();
                self.handle_ipv6_packet(packet);
            },
            _ => {
                warn!("Unknown protocol received!");
            }
        }
    }

    fn handle_ipv4_packet(&self, packet: Ipv4Packet) {
        
    }

    fn handle_ipv6_packet(&self, packet: Ipv6Packet) {

    }

    fn handle_udp_packet(&self, packet: UdpPacket, ip: String) {

    }
}