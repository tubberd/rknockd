use std::collections::HashMap;

use serde::*;

#[derive(Deserialize)]
pub struct Config {
    pub log_file: Option<String>,
    pub interface: String,
    pub triggers: HashMap<String, Trigger>
}

#[derive(Deserialize)]
pub struct Trigger {
    pub sequence: Vec<u16>,
    pub sequence_timeout: u64,
    pub command: String,
    pub command_timeout: Option<u64>,
    pub stop_command: Option<String>
}